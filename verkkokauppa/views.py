from django.shortcuts import render, redirect
from django.db.models import Q
from verkkokauppa.utils import getProductObject, getTotalCartPrice
from .models import Product

# Create your views here.

def index(request):
    request.session.set_expiry(900)
    return render(request,'verkkokauppa/frontpage.html')

def storeUI(request):
    filter = request.GET.get('orderBy',None)
    if filter is not None:
        clickCount = request.session.get('clickCount',1)
        if clickCount % 2 == 0:
            products = Product.objects.all().order_by(filter)
        else:
            products = Product.objects.all().order_by(filter).reverse()
        request.session['clickCount'] = clickCount+1
    else:
        products = Product.objects.all().order_by('name')

    context = {
        'products': products,
        'clicked': 1
    }
    
    return render(request,'verkkokauppa/store.html', context)

def product(request, id):
    product = Product.objects.get(id=id)
    context = {
        'product': product
    }

    return render(request,'verkkokauppa/product.html', context)

def search(request):
    querySet = []
    query = request.GET.get('q')
    filter = request.GET.get('orderBy')
    queries = query.split(" ")
    clickCount = request.session.get('clickCount',1)
    for q in queries:
        if clickCount % 2 == 0:
            products = Product.objects.filter(
                Q(name__icontains=q) | Q(price__icontains=q) | Q(platform__icontains=q) | Q(amount__icontains=q)
            ).distinct().order_by(filter)
        else:
            products = Product.objects.filter(
                Q(name__icontains=q) | Q(price__icontains=q) | Q(platform__icontains=q) | Q(amount__icontains=q)
            ).distinct().order_by(filter).reverse()
    request.session['clickCount'] = clickCount+1
    for product in products:
            querySet.append(product)
    context = {
        'products':querySet,
        'q': query
    }
    return render(request,'verkkokauppa/search.html', context)

def addToCart(request):
    products = request.session.get('products', [])
    obj = getProductObject(request)
    products.append(obj)
    totalPrice = getTotalCartPrice(products)
    context = {
        'products': products,
        'totalPrice': round(totalPrice,2),
    }
    request.session['products'] = products
    return render(request,'verkkokauppa/cart.html', context)

def deleteFromCart(request):
    products = request.session.get('products', [])
    obj = getProductObject(request)
    products.pop(products.index({'name':obj['name'], 'price': obj['price'], 'platform': obj['platform'], 'id': obj['id']}))
    totalPrice = getTotalCartPrice(products)
    context = {
        'products':products,
        'totalPrice': round(totalPrice,2)
    }
    request.session['products'] = products
    return redirect('/cart/') 

def getCart(request):
    products = request.session.get('products', [])
    totalPrice = getTotalCartPrice(products)
    context = {
        'products':products,
        'totalPrice': round(totalPrice,2)
    }
    return render(request,'verkkokauppa/cart.html', context)


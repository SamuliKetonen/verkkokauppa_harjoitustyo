from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('store/', views.storeUI, name='storeUI'),
    path('products/<int:id>', views.product, name='product'),
    path('store/search/', views.search, name='search'),
    path('addToCart/',views.addToCart, name='addToCart'),
    path('deleteFromCart/',views.deleteFromCart, name='deleteFromCart'),
    path('cart/', views.getCart, name='getCart'),
]

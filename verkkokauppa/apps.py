from django.apps import AppConfig


class VerkkokauppaConfig(AppConfig):
    name = 'verkkokauppa'

# Generated by Django 3.1.2 on 2020-10-08 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('verkkokauppa', '0006_product_img'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='img',
            field=models.ImageField(upload_to='static/verkkokauppa/'),
        ),
    ]

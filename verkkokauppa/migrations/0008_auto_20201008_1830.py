# Generated by Django 3.1.2 on 2020-10-08 15:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('verkkokauppa', '0007_auto_20201008_1759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='img',
            field=models.ImageField(default='static/verkkokauppa/placeholder.png', upload_to=''),
        ),
    ]

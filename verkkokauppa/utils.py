def getProductObject(request):
    obj = {
        'name' : request.POST['name'],
        'price' : request.POST['price'],
        'platform': request.POST['platform'],
        'id': request.POST['id'],
    }
    return obj

def getTotalCartPrice(products):
    totalPrice = 0
    for product in products:
        totalPrice = totalPrice + float(product['price'])
    return totalPrice
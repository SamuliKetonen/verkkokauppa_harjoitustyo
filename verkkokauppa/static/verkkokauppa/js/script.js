$(document).ready(function () {
    $('#addToCart').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            url: '/addToCart/',
            data: {
                name: $('#name').val(),
                price: $('#price').val(),
                platform: $('#platform').val(),
                id: $('#id').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function () {
                $('#added').attr('style', "display:block")
            },
        });

    });
});
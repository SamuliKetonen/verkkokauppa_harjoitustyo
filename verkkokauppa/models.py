from django.db import models

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=4,decimal_places=2)
    description = models.TextField()
    platform = models.CharField(max_length=50)
    amount = models.IntegerField()
    img = models.ImageField(upload_to='',default='placeholder.png')
    def __str__(self):
        return self.name
